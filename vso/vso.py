from sunpy.net import vso

# create a new VSOClient instance
client = vso.VSOClient()

# build our query
result = client.query(
    vso.attrs.Time((2001, 9, 20, 8), (2011, 9, 20, 9)),
    vso.attrs.Instrument('eit')
)

# print the number of matches
print("Number of records found: %d " % result.num_records())
print result

# download matches to /download/path
#res = client.get(result, path="/home/jgood/SUN/down/{file}").wait()
