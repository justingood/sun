import sunpy
from matplotlib import pyplot as plt

aia = sunpy.Map(sunpy.AIA_171_IMAGE)
rhessi = sunpy.Map(sunpy.RHESSI_IMAGE)

fig = plt.figure()
axes = fig.add_subplot(111)

aia_extent = aia.xrange + aia.yrange
rhessi_extent = rhessi.xrange + rhessi.yrange

im2 = plt.imshow(rhessi, extent=rhessi_extent, cmap=rhessi.cmap, origin='lower', norm=rhessi.norm(), zorder=10)
im1 = plt.imshow(aia, extent=aia_extent, cmap=aia.cmap, origin='lower', norm=aia.norm(), zorder=1)

plt.show()
