#!/usr/bin/python

import sunpy
from sunpy.net.helioviewer import HelioviewerClient

hv = HelioviewerClient()
datasources = hv.get_data_sources()

sunpy.config.set('downloads', 'download_dir', '/home/jgood/SUN/down')

# print a list of datasources and their associated ids
#for observatory, instruments in datasources.items():
#    for inst, detectors in instruments.items():
#            for det, measurements in detectors.items():
#	                for meas, params in measurements.items():
#			                print("%s %s: %d" % (observatory, params['nickname'], params['sourceId']))

hv.download_png('2000/01/01 00:30:00', 4.8, "[13,1,100]", x0=0, y0=0, width=1920, height=1200)
