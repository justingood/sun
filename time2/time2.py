#!/usr/bin/env python
import datetime

now = datetime.datetime.now()
 
def daterange( start_date, end_date ):
    if start_date <= end_date:
        #for n in range( ( end_date - start_date ).days + 1 ):
        for n in range( ( end_date - start_date ).days + 1 ):
            yield start_date + datetime.timedelta( n )
    else:
        #for n in range( ( start_date - end_date ).days + 1 ):
        for n in range( ( start_date - end_date ).days + 1 ):
            yield start_date - datetime.timedelta( n )
 
start = datetime.datetime( year = 2010, month = 6, day = 2 )
end = datetime.datetime( year = now.year, month = now.month, day = now.day, hour = now.hour, minute = now.minute )
 
for date in daterange( start, end ):
    for hour in range(0,25):
        padhour = "{0:02d}".format(hour)
        for minute in range(0,60):
                padmin = "{0:02d}".format(minute)
                thetime = (padhour + ':' + padmin + ':00')
		print date
		#print (str(date) + ' ' + thetime)
