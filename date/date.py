import datetime

begin_year = datetime.date(2010, 1, 1)
end_year = datetime.date(2012, 12, 31)
one_day = datetime.timedelta(days=1)

next_day = begin_year

for day in range(0, 366):  # includes potential leap year
    if next_day > end_year:
        break
    print next_day.strftime("%Y/%m/%d")
    # increment date object by one day
    next_day += one_day
